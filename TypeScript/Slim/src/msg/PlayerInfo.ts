/**
 * 存放玩家信息（等级、经验、金钱等等）
 */
class PlayerInfo {

    //一些临时常量

    /** 决心最大值*/
    public MaxHeart = 100;

    /** 点击决心增量*/
    private HeartAddNum = 20;

    /** 基础运动量*/
    private BaseExercise = 10;

    /** 阻力*/
    private Bound = 20;

    /** 时间点击增量*/
    private TimeAddNum = 5000;


    //常量

    /** 当前等级*/
    public curLv: number;

    /** 当前体重*/
    public curWeight: number;

    /** 决心保持时间*/
    public curHeartTime: number;

    /** 当前决心值*/
    public curHeart: number;

    /** 当前运动速度*/
    public get curExercise(): number {
        let coe = this.curHeart / this.Bound > 1 ? this.curHeart / this.Bound : 1;
        return this.BaseExercise * coe * UpgradeSys.GetInstance().equipCoe;
    };

    /** 当前卡路里*/
    public curCalorie: number;

    /** 当前体重的最大卡路里*/
    public curMaxCalorie: number;

    /**
     * 当前魅力（决定每秒获取多少金钱）
     */
    public get curCharm(): number {
        return Math.floor((500 - this.curWeight) * UpgradeSys.GetInstance().charmCoe);
    }

    /**
     * 金钱数量
     */
    public curGoldNum: number;

    public static instance: PlayerInfo;
    public static GetInstance(): PlayerInfo {
        if (this.instance == undefined) {
            this.instance = new PlayerInfo();
        }
        return this.instance;
    }

    constructor() {
        // this.Init()
    }

    public Init() {
        this.curLv = 1;
        this.curWeight = 499;
        this.curHeart = 0;
        this.curCalorie = 500;
        this.curMaxCalorie = 500;
        this.curHeartTime = 0;
        this.curGoldNum = 0;

        Laya.timer.frameLoop(1, this, this.Update);
    }

    /**
     * 增加决心
     */
    public AddHeart(addNum: number) {
        this.curHeart += addNum;
        if (this.curHeart > this.MaxHeart) {
            let outNum = this.curHeart - this.MaxHeart;
            let outTime = Math.floor(outNum / this.HeartAddNum * this.TimeAddNum);

            this.curHeartTime += outTime;
            this.curHeart = this.MaxHeart;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange);
        }
        EventSys.GetInstance().AddEvent(Events.HeartChange);
    }

    /**
     * 卡路里增加
     */
    public AddCalorie(changeValue: number) {
        if (changeValue <= 0)
            return;

        this.curCalorie += changeValue;
        while (this.curCalorie > this.curMaxCalorie) {
            this.curWeight++;
            this.curCalorie -= this.curMaxCalorie;
            this.curMaxCalorie = 500;
            EventSys.GetInstance().AddEvent(Events.CharmChange)
        }

        EventSys.GetInstance().AddEvent(Events.CalorieChange);
    }

    /**
     * 卡路里减少
     * @param changeValue 
     */
    public DecCalorie(changeValue: number) {
        if (changeValue <= 0)
            return;

        this.curCalorie -= changeValue;
        while (this.curCalorie <= 0) {
            this.curWeight--;
            this.curMaxCalorie = 500;
            this.curCalorie += this.curMaxCalorie;
            EventSys.GetInstance().AddEvent(Events.CharmChange)
        }

        EventSys.GetInstance().AddEvent(Events.CalorieChange);
    }

    /**
     * 增加金钱
     */
    public AddGold(addNum: number) {
        this.curGoldNum += addNum;
        EventSys.GetInstance().AddEvent(Events.GoldNumChange);
    }

    /**
     * 花费金钱，返回是否扣除成功
     */
    public CostGold(costNum: number): boolean {
        if (this.curGoldNum < costNum)
            return false;
        this.curGoldNum -= costNum;
        EventSys.GetInstance().AddEvent(Events.GoldNumChange);
        return true;
    }

    /**
     * 点击中间的胖子增加决心
     */
    public ClickPerson() {
        if (this.curHeartTime > 0) {
            this.curHeartTime += this.TimeAddNum;
            this.curHeartTime = this.curHeartTime > UpgradeSys.GetInstance().maxHeartTime ?
                UpgradeSys.GetInstance().maxHeartTime : this.curHeartTime;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange)
        }
        else {
            this.AddHeart(this.HeartAddNum);
        }
    }


    /**
     * 获取当前体重（显示在主界面上）
     */
    public GetWeightStr(): string {
        let weight = this.curWeight;
        if (this.curCalorie == this.curMaxCalorie)
            weight++
        let weightStr = this.curWeight + '';
        let calorieStr = (this.curCalorie / this.curMaxCalorie) + '';

        let strList = calorieStr.split('.')
        if (strList.length > 1) {
            calorieStr = strList[1];
            calorieStr = calorieStr.substr(0, 2);
            weightStr = weightStr + '.' + calorieStr;
        }

        return weightStr
    }


    /**
     * 每帧减少决心
     */
    private UpdateDecHeart() {
        if (this.curHeartTime > 0) {
            this.curHeartTime -= Laya.timer.delta;
            this.curHeartTime = this.curHeartTime < 0 ? 0 : this.curHeartTime;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange);
        }
        else if (this.curHeart > 0) {
            let decNum = UpgradeSys.GetInstance().heartDec / 1000 * Laya.timer.delta;
            this.curHeart -= decNum;
            this.curHeart = this.curHeart < 0 ? 0 : this.curHeart;

            EventSys.GetInstance().AddEvent(Events.HeartChange);
        }
    }

    /**
     * 每秒减少卡路里
     */
    private UpdateDecCalorie() {
        this.DecCalorie(this.curExercise);
    }

    /**
     * 每秒增加金钱
     */
    private UpdateAddGold() {
        this.AddGold(this.curCharm);
    }

    private updatePerSecTime: number = 0;
    /**
     * 每秒执行一次
     */
    private UpdatePerSec() {
        this.UpdateDecCalorie();
        this.UpdateAddGold();
    }

    private Update() {
        if (Laya.timer.currTimer - this.updatePerSecTime >= 1000) {
            this.UpdatePerSec();
            this.updatePerSecTime = Laya.timer.currTimer;
        }

        this.UpdateDecHeart();
    }
}