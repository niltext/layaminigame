/**
 * 消息队列
 */
class EventSys {

    /** 监听函数列表*/
    private handleDic: {};

    /** 消息队列*/
    private eventList: Events[];

    public static instance: EventSys;
    public static GetInstance(): EventSys {
        if (this.instance == undefined) {
            this.instance = new EventSys();
        }
        return this.instance;
    }

    constructor() {
        this.Init();
    }

    private Init() {
        this.handleDic = {};
        this.eventList = [];

        Laya.timer.frameLoop(1, this, this.Update);
    }

    /**
     * 添加监听函数
     */
    public AddHandler(eventId: Events, handle: Laya.Handler) {
        if (this.handleDic[eventId] == undefined) {
            this.handleDic[eventId] = [];
        }
        this.handleDic[eventId].push(handle);
    }

    /**
     * 发送消息
     */
    public AddEvent(eventId: Events) {
        this.eventList.push(eventId);
    }

    /**
     * 发送消息，立刻执行
     */
    public AddEventNow(eventId: Events) {
        let handleList = <Laya.Handler[]>this.handleDic[eventId];
        if (handleList == undefined)
            return;
        for (let i = 0; i < handleList.length; i++) {
            try {
                handleList[i].run()
            } catch (error) {
                console.error('event error ', eventId, error)
            }
        }
    }

    /**
     * 移除一个监听事件
     */
    public RemoveHandler(eventId: Events, handle: Laya.Handler) {
        if (this.handleDic[eventId] == undefined) {
            return;
        }
        let index = this.handleDic[eventId].indexOf(handle);
        if (index > -1) {
            this.handleDic[eventId].splice(index, 1);
        }
    }

    private Update() {
        if (this.eventList.length > 0)
            console.log('eventList ', this.eventList);
        while (this.eventList.length > 0) {
            let eventId = this.eventList.pop();

            let handleList = <Laya.Handler[]>this.handleDic[eventId];
            if (handleList == undefined)
                return;
            for (let i = 0; i < handleList.length; i++) {
                try {
                    handleList[i].run()
                } catch (error) {
                    console.error('event error ', eventId, error)
                }
            }
        }
    }
}

/**
 * 事件类型
 */
enum Events {
    /** 锻炼速度改变*/
    ExerciseSpeedChange,
    /** 卡路里改变*/
    CalorieChange,
    /** 金钱数量改变*/
    GoldNumChange,
    /** 决心改变*/
    HeartChange,
    /** 持续时间改变*/
    HeartTimeChange,
    /**
     * 魅力改变
     */
    CharmChange,

    Count
}