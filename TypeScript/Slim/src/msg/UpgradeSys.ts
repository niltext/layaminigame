/**
 * 升级系统
 */
class UpgradeSys {

    //测试数据

    public skillList: {
        name: string,
        baseMoney: number,
        addMoney: number,
        des1: string,
        des2: string,
        addValue: number,
    }[] = [
        {
            name: '器材',
            baseMoney: 100,
            addMoney: 20,
            des1: '器材1',
            des2: '器材2',
            addValue: 1,
        },
        {
            name: '沐浴房',
            baseMoney: 200,
            addMoney: 10,
            des1: '沐浴房1',
            des2: '沐浴房2',
            addValue: -0.01,
        },
        {
            name: '会员卡',
            baseMoney: 300,
            addMoney: 10,
            des1: '会员卡1',
            des2: '会员卡2',
            addValue: 10000,
        },
        {
            name: '助教',
            baseMoney: 400,
            addMoney: 5,
            des1: '助教1',
            des2: '助教2',
            addValue: 0.1,
        }
    ]

    private baseEquipCoe = 5;
    private baseHeartDec = 1;
    private baseMaxHeartTime = 3600000;
    private baseCharmCoe = 1;

    //测试数据
    public skillDataList: SkillData[];

    /**
     * 器材增加的运动量
     */
    public get equipCoe(): number {
        return this.skillDataList[0].attr + this.baseEquipCoe;
    }

    /**
     * 沐浴房增加的决心衰减速度
     */
    public get heartDec(): number {
        return this.skillDataList[1].attr + this.baseHeartDec;
    }

    /**
     * 会员卡增加的持续时间
     */
    public get maxHeartTime(): number {
        return this.skillDataList[2].attr + this.baseMaxHeartTime;
    }

    /**
     * 助教增加的魅力
     */
    public get charmCoe(): number {
        return this.skillDataList[3].attr + this.baseCharmCoe;
    }

    public static instance: UpgradeSys;
    public static GetInstance(): UpgradeSys {
        if (this.instance == undefined) {
            this.instance = new UpgradeSys();
        }
        return this.instance;
    }

    constructor() {
        this.Init()
    }

    public Init() {
        this.skillDataList = [];
        for (let i = 0; i < this.skillList.length; i++) {
            this.skillDataList.push(new SkillData(this.skillList[i]))
        }
    }

    public AddLv(index: number) {
        if (index > -1 && index < this.skillDataList.length) {
            let skillData = this.skillDataList[index];

            let costMoneyNum = skillData.needMoney;
            if (PlayerInfo.GetInstance().CostGold(costMoneyNum)) {
                this.skillDataList[index].lv++;

                switch (index) {
                    case 0:
                        EventSys.GetInstance().AddEvent(Events.ExerciseSpeedChange)
                        break;
                    case 3:
                        EventSys.GetInstance().AddEvent(Events.CharmChange)
                        break;
                    default:
                        break;
                }
            }
        }
    }
}


class SkillData {
    public data: {
        name: string,
        baseMoney: number,
        addMoney: number,
        des1: string,
        des2: string,
        addValue: number,
    }

    private _lv: number;

    public get lv(): number {
        return this._lv;
    }

    public set lv(value: number) {
        if (this._lv != value) {
            this._lv = value;
            this.attr = this.data.addValue * this.lv;
            this.needMoney = this.data.addMoney * this.lv + this.data.baseMoney;
        }
    }

    public attr: number

    public needMoney: number;

    constructor(data: {
        name: string,
        baseMoney: number,
        addMoney: number,
        des1: string,
        des2: string,
        addValue: number,
    }) {
        this.data = data;
        this.lv = 0;
    }
}