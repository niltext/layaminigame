class NetSys {

    private httpUrl:string;

    private tcpUrl:string;

    /** 当前建立的链接*/
    private socket:Laya.Socket;

    constructor() {
        this.Init()
    }

    private Init()
    {
        
    }

    /**
     * 建立链接，设置监听函数
     */
    private InitSocket()
    {
        this.socket = new Laya.Socket();
        this.socket.on(Laya.Event.OPEN,this,()=>
        {
            console.log('connect suc');
        });
        this.socket.on(Laya.Event.CLOSE,this,()=>
        {
            console.log('connect close');
        });
        this.socket.on(Laya.Event.ERROR,this,()=>
        {
            console.error('connect fail');
        });
        this.socket.on(Laya.Event.MESSAGE,this,()=>
        {
            console.log('receive message');
        });

        this.socket.connectByUrl(this.tcpUrl);
    }
}