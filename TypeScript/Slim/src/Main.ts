// 程序入口
class Main{
    constructor()
    {
        Laya.MiniAdpter.init();
        let realHeight = 720 * (Laya.Browser.clientHeight / Laya.Browser.clientWidth);
        Laya.init(720, realHeight);
        this.initStage();
        this.loadResource();
        PlatformMgr.GetInstance().init();
    }

    //初始化stage
    private initStage(): void
    {
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_WIDTH;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
    }

    //加载资源
    private loadResource(): void
    {
        let uiResArr:Array<any> = [
            { url: "res/atlas/common.atlas", type:Laya.Loader.ATLAS},
            { url: "res/atlas/main.atlas", type:Laya.Loader.ATLAS},
            { url: "res/atlas/levelUp.atlas", type:Laya.Loader.ATLAS}
        ];
        Laya.loader.load(uiResArr, Laya.Handler.create(this, () => {
			UIMgr.GetInstance().init();
            PlayerInfo.GetInstance().Init();
			view.page.HallView.onShowMe();
            // UIMgr.closeUI(view.page.HallView.instance);
        }));
    }
}
new Main();