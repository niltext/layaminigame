/**
 * @author hjw
 * Tween、时间线工具集
 */
import Tween = laya.utils.Tween;
import Handler = Laya.Handler;
import Component = Laya.Component;
import Ease = Laya.Ease;
import TimeLine = Laya.TimeLine;

class EffectUtils
{
    public static CURRENTWIDTH:number = 0;
    public static CURRENTHEIGHT:number = 0;

    constructor() {

    }


    /**
     * 缓动打开弹窗
     * @param target
    */
    public static openDialog(target:Component, compH:Handler = null):void
    {
        target.cacheAs = "none";//"normal";
        target&&(target.mouseEnabled = false);
        target.pivot(target.width/2,target.height/2);
        target.pos(EffectUtils.CURRENTWIDTH/UIMgr.scale/2,EffectUtils.CURRENTHEIGHT/UIMgr.scale/2);
        target.scale(0,0);
        Tween.to(target,{ scaleX: 1, scaleY:1}, 250, Ease.backOut,Handler.create(EffectUtils,()=>{
            if (target) {
                target.mouseEnabled = true;
                target.cacheAs = "none";
                compH && compH.run();
            }
        }));
    }

    /**
     * 缓动关闭弹窗
     * @param target
    */
    public static closeDialog(target:Component, callBack:Handler = null):void
    {
        target&&(target.mouseEnabled = false);
        Tween.to(target, {scaleX: 0, scaleY: 0}, 250, Ease.backIn, Handler.create(EffectUtils,()=>{
            target&&(target.mouseEnabled = true);
            callBack&&callBack.run();
        }));
    }

    /**
     * 渐显渐隐
     * @param	target
     * @return
     */
    public static emer(target:Component, stayTime:number, delay:number = 0):void
    {
        target.alpha = 0;
        let tl:TimeLine = new TimeLine();
        tl.reset();
        tl.to(target, {alpha: 0}, delay)
            .to(target, {alpha: 1}, 300)
            .to(target, {alpha: 1}, stayTime)
            .to(target, {alpha: 0}, 200);
        tl.play();
        //return tl;
    }
}