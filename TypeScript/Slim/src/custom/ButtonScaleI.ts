/**
 * @author hjw
 * 按钮扩展组件
 */
namespace custom{
    import Button = laya.ui.Button;
    import Event = laya.events.Event;
    import SoundManager = laya.media.SoundManager;

    export class ButtonScaleI extends Button {
        constructor(skin:string="", label:string="") {
            super(skin, label);
            this.anchorX = this.anchorY = 0.5;
            this.stateNum = 1;
            this.initEvent();
        }

        /**按钮点击播放声音类型（类型按表数据索引来定）**/
        public soundType:number =  0;
        /**鼠标或手指按下的持续时间**/
        public scaleDownTime:number = 100;
        /**鼠标或手指抬起的持续时间**/
        public scaleUpTime:number = 200;
        private bIsMove:Boolean;

        /**初始化事件**/
        private initEvent():void
        {
            this.on(Event.MOUSE_DOWN, this, this.scaleSmall);
            this.on(Event.MOUSE_UP, this, this.scaleNormal);
            this.on(Event.MOUSE_OUT, this, this.scaleNormal);
        }

        /**鼠标按下缩放效果**/
        private scaleSmall():void{
            if(this.bIsMove) return;
            this.bIsMove = true;
            // let str:string;
            // if (this.soundType != 0 && ConfigData.soundCfg[this.soundType]){
            //     (str = ConfigData.soundCfg[this.soundType].src) && SoundManager.playSound(str);
            // }
            Tween.to(this, {scaleX:1.08, scaleY: 1.08}, this.scaleDownTime);
        }
        /**鼠标抬起放大恢复效果**/
        private scaleNormal():void{
            Tween.to(this, {scaleX:1, scaleY:1}, this.scaleUpTime, ButtonScaleI.backOut, Handler.create(this,()=>{
                this.bIsMove = false;
            }));
        }

        public static backOut(t:number, b:number, c:number, d:number, s:number = 10):number {
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        }
    }
}