/**
 * @author hjw
 * 主界面
 */
namespace view.page {
    import Event = laya.events.Event;

    export class HallView extends ui.hall.HallUI {
        constructor() {
            super();
            this.name = "HallView";
            this.resList = [
                { url: "res/atlas/main.atlas", type: Laya.Loader.ATLAS }
            ];
        }

        public onCreated(): void {
            super.onCreated();
            this.Init();
        }

        public onOpen(): void {
            super.onOpen();
            //添加到父容器
            super.addToParent(this);
            this.btmBox.y = UIMgr.REH - 160;

            this.btn_levelSys.on(Event.CLICK, this, LevelUp.onShowMe);
        }

        private static isOpening: Boolean;
        public static instance: HallView;
        public static onShowMe(): void {
            if (HallView.isOpening) return;
            HallView.isOpening = true;
            if (HallView.instance == undefined) {
                HallView.instance = new HallView();
            }
            UIMgr.openUI(HallView.instance, UIMgr.LAYER_POP2);
        }

        public onClose(): void {
            this.btn_levelSys.off(Event.CLICK, this, LevelUp.onShowMe);
            HallView.isOpening = false;
            super.onClose();
        }

        /**
         * 创建界面时初始化
         */
        public Init() {
            this.SetEventHandler();
            this.SetBtnHandler();
            this.RefreshView();
        }

        /**
         * 添加事件监听函数
         */
        private SetEventHandler() {
            EventSys.GetInstance().AddHandler(Events.GoldNumChange, new Laya.Handler(this, this.RefreshMoney));
            EventSys.GetInstance().AddHandler(Events.HeartChange, new Laya.Handler(this, () => {
                this.RefreshExercise();
                this.RefreshHeart();
            }));
            EventSys.GetInstance().AddHandler(Events.ExerciseSpeedChange, new Laya.Handler(this, this.RefreshExercise));
            EventSys.GetInstance().AddHandler(Events.HeartTimeChange, new Laya.Handler(this, this.RefreshDuringTime));
            EventSys.GetInstance().AddHandler(Events.CalorieChange, new Laya.Handler(this, this.RefreshWeight))
            EventSys.GetInstance().AddHandler(Events.CharmChange, new Laya.Handler(this, this.RefreshCharm));
        }

        /**
         * 添加点击事件
         */
        private SetBtnHandler() {
            this.playerRoot.on(Laya.Event.CLICK, this, () => {
                PlayerInfo.GetInstance().ClickPerson();
            })

        }

        /**
         * 刷新界面显示
         */
        private RefreshView() {
            this.RefreshCharm();
            this.RefreshDuringTime();
            this.RefreshExercise();
            this.RefreshMoney();
            this.RefreshWeight();
            this.RefreshHeart();
        }

        /**
         * 刷新金钱数量显示
         */
        private RefreshMoney() {
            this.money.text = PlayerInfo.GetInstance().curGoldNum + '';
        }

        /**
         * 刷新魅力显示
         */
        private RefreshCharm() {
            this.charm.text = PlayerInfo.GetInstance().curCharm + '/S';
        }

        /**
         * 刷新决心进度条
         */
        private RefreshHeart() {
            this.pointer.x = 558 * PlayerInfo.GetInstance().curHeart / PlayerInfo.GetInstance().MaxHeart
        }

        /**
         * 刷新剩余时间
         */
        private RefreshDuringTime() {
            let time = Math.floor(PlayerInfo.GetInstance().curHeartTime / 1000);
            let hour = Math.floor(time / 3600);
            let min = Math.floor((time % 3600) / 60);
            let sec = time % 60;
            this.time.text = '坚持时间' + hour + '小时' + min + '分' + sec + '秒';
        }

        /**
         * 刷新剩余体重
         */
        private RefreshWeight() {
            this.weight.text = PlayerInfo.GetInstance().GetWeightStr() + '斤';
            this.leftCalorie.text = '剩余' + Math.ceil(PlayerInfo.GetInstance().curCalorie) + '卡路里'
        }

        /**
         * 刷新锻炼速度显示
         */
        private RefreshExercise() {
            this.calorie.text = Math.ceil(PlayerInfo.GetInstance().curExercise) + '/S'
        }

    }
}