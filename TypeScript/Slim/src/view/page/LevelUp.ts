namespace view.page {
    import Event = laya.events.Event;

    export class LevelUp extends ui.hall.LevelUpUI {
        constructor() {
            super();
            this.name = "LevelUp";
            this.resList = [
                { url: "res/atlas/levelUp.atlas", type: Laya.Loader.ATLAS }
            ];
        }

        public onCreated(): void {
            super.onCreated();
        }

        public onOpen(): void {
            super.onOpen();
            //添加到父容器
            super.addToParent(this);

            EffectUtils.openDialog(this.myBox);

            this.btnClose.on(Event.CLICK, this, this.OnBtnClose);
            this.bg.on(Event.CLICK, this, this.OnBtnClose);

            this.Init();
        }

        private static isOpening: Boolean;
        public static instance: LevelUp;
        public static onShowMe(): void {
            if (LevelUp.isOpening) return;
            LevelUp.isOpening = true;
            if (LevelUp.instance == undefined) {
                LevelUp.instance = new LevelUp();
            }
            UIMgr.openUI(LevelUp.instance, UIMgr.LAYER_POP2);
        }

        private OnBtnClose(): void {
            this.bg.off(Event.CLICK, this, this.OnBtnClose);
            this.btnClose.off(Event.CLICK, this, this.OnBtnClose);
            EffectUtils.closeDialog(this.myBox, Handler.create(this, this.onTweenBtnClose));
        }

        private onTweenBtnClose(): void {
            UIMgr.closeUI(this);
        }

        public onClose(): void {
            LevelUp.isOpening = false;
            super.onClose();
        }

        public Init() {
            this.skillList.renderHandler = new Laya.Handler(this, this.ItemRender);

            this.RefreshList();
        }

        public RefreshList() {
            let length = UpgradeSys.GetInstance().skillDataList.length;
            let list = [];
            list.length = length;
            this.skillList.dataSource = list;
        }

        private ItemRender(cell: Laya.Box, index: number) {
            let upgradeBtn = cell.getChildByName('btn_levelUp')
            let needMoney = <Laya.Label>cell.getChildByName('needMoney')
            let explain1 = <Laya.Label>cell.getChildByName('explain1')
            let equipment = <Laya.Label>cell.getChildByName('equipment')
            let explain2 = <Laya.Label>cell.getChildByName('explain2')

            let skillData = UpgradeSys.GetInstance().skillDataList[index];

            needMoney.text = skillData.needMoney + '';
            explain1.text = skillData.data.des1;
            explain2.text = skillData.data.des2;
            equipment.text = skillData.data.name;

            upgradeBtn.on(Laya.Event.CLICK, this, this.OnUpgradeClick, [index]);
        }

        private OnUpgradeClick(index)  {
            UpgradeSys.GetInstance().AddLv(index);
            this.RefreshList();
        }
    }
}