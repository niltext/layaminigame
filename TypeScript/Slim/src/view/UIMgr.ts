/**
 * @author hjw
 * 层级窗口管理
 */
import Box = laya.ui.Box;
class UIMgr {

    /**游戏所有层级 容器*/
    public static AllLayer: Box;
    /**背景层**/
    //public static let bgBox:Box;

    public static LAYER_BG: string = "layer_bg";//UI背景
    public static LAYER_POP1: string = "layer_pop1";
    public static LAYER_POP2: string = "layer_pop2";
    public static LAYER_GUIDE: string = "layer_guide";//引导层
    public static LAYER_TIPS: string = "layer_tips";//弹窗层
    public static LAYER_LOADING: string = "layer_loading";
    public static LAYER_SHARE: string = "layer_share";
    private static _layerNames: Array<string>;
    private static _layers: Object;

    //真机缩放率
    public static scaleRatio:number;
    public static REH:number;

    /**
     * 正处于打开状态的UI界面数组（按层分组）
     */
    public static openingUIArray: Object;
    /**
     * 已经创建的ui缓存对象集合
     */
    protected static uiMap: Object;
    public static uiNum: number = 0;
    public static instance: UIMgr;

    public static GetInstance(): UIMgr {
        if (UIMgr.instance == undefined) {
            UIMgr.instance = new UIMgr();
        }
        return UIMgr.instance;
    }

    public init(): void {
        UIMgr.scaleRatio = Laya.Browser.clientWidth / 720;
        UIMgr.REH = 720 * (Laya.Browser.clientHeight / Laya.Browser.clientWidth);

        UIMgr.AllLayer = new Box();
        UIMgr.AllLayer.mouseEnabled = true;

        Laya.stage.addChild(UIMgr.AllLayer);
        UIMgr.AllLayer.top = UIMgr.AllLayer.bottom = UIMgr.AllLayer.left = UIMgr.AllLayer.right = 0;
        UIMgr._layerNames = [UIMgr.LAYER_BG, UIMgr.LAYER_POP1, UIMgr.LAYER_POP2, UIMgr.LAYER_GUIDE, UIMgr.LAYER_TIPS, UIMgr.LAYER_LOADING, UIMgr.LAYER_SHARE];
        UIMgr._layers = {};
        UIMgr.uiMap = {};
        UIMgr.openingUIArray = {};

        let __layer: Box = new Box();
        __layer.name = UIMgr.LAYER_BG;
        __layer.mouseThrough = true;
        __layer.mouseEnabled = true;
        __layer.left = __layer.top = __layer.right = __layer.bottom = 0;
        UIMgr.AllLayer.addChild(__layer);
        UIMgr._layers[UIMgr._layerNames[0]] = __layer;
        UIMgr.openingUIArray[UIMgr._layerNames[0]] = [__layer];

        for (let i: number = 1; i < UIMgr._layerNames.length; i++) {
            __layer = new Box();
            __layer.name = UIMgr._layerNames[i];
            __layer.mouseThrough = true;
            __layer.mouseEnabled = true;
            __layer.left = __layer.top = __layer.right = __layer.bottom = 0;
            UIMgr.AllLayer.addChild(__layer);
            UIMgr._layers[UIMgr._layerNames[i]] = __layer;
            UIMgr.openingUIArray[UIMgr._layerNames[i]] = [];
        }
        this.onStageResize();
    }

    public static scale:number;
    private onStageResize(): void {
        UIMgr.scale = Math.min(Laya.stage.width / Laya.stage.designWidth, Laya.stage.height / Laya.stage.designHeight);
        for (let key in UIMgr._layers) {
            // @ts-ignore
            let _layer: Box = UIMgr._layers[key];
            _layer.scale(UIMgr.scale, UIMgr.scale);
        }
        //bgBox.scale(scale, scale);
        //console.log('缩放比例:' + scale);
        if(Laya.stage.width<Laya.stage.height)
        {
            EffectUtils.CURRENTWIDTH = Laya.stage.width;
            EffectUtils.CURRENTHEIGHT = Laya.stage.height;
        }else{
            EffectUtils.CURRENTWIDTH = Laya.stage.height;
            EffectUtils.CURRENTHEIGHT = Laya.stage.width;
        }
    }

    /**
     * 添加页面
     * @param uiname
     * @param layerName
     * @param isShowLoad 打开页面时是否显示loading动画
     * @return
     */
    public static openUI(uiname: any, layerName: string = UIMgr.LAYER_POP2, isShowLoad: Boolean = true): String {
        UIMgr.uiNum++;
        let _ui: UIBase0;
        let isOpen: Boolean = !isShowLoad;
        if (UIMgr.isOpening(uiname, layerName)) {
            _ui = UIMgr.uiMap[uiname.name];
            _ui.isOpen = isOpen = false;
        }
        let closeUIName: string = UIMgr.autoCloseUI(layerName, false);
        UIMgr._createUI(uiname, layerName, isOpen);
        return closeUIName;
    }

    /**
     * 自动关闭层级中最后打开的界面
     * @param	layerName  层级名字
     * @param	isRealClose  是否真的关闭页面
     * @return
     */
    public static autoCloseUI(layerName:string, isRealClose:Boolean):string
    {
        let layer:Box = UIMgr.getLayer(layerName);
        let maxCount :number = parseInt(layer['maxcount']);
        if (isNaN(maxCount) || maxCount < 1) maxCount = 1;
        if (UIMgr.openingUIArray[layerName].length >= maxCount) {
            let _ui :UIBase0 = UIMgr.openingUIArray[layerName][maxCount-1];
            let uiName :string = _ui.name;
            isRealClose && UIMgr.closeUI(_ui);
            return uiName;
        }
        return '';
    }

    protected static _createUI(uiname: UIBase0, layerName: string, isOpen: Boolean = false): void {
        let layer: Box = UIMgr.getLayer(layerName);
        let _ui: UIBase0 = UIMgr.uiMap[uiname.name];
        let hasResToLoad: Boolean = false;
        if (_ui == null || !_ui.isloaded) {
            _ui = UIMgr.__cr(uiname);
            _ui.uiNum = UIMgr.uiNum;
            _ui.isCrOpen = true;
            _ui.layerName = layerName;
            _ui.layerParent = layer;//存父亲
            hasResToLoad = _ui.loadRes();
            _ui.isloaded = true;
        } else {
            for (let i: number = 0, sz: number = _ui.resList.length; i < sz; i++) {
                if (!Laya.loader.getRes(_ui.resList[i].url)) {
                    hasResToLoad = _ui.loadRes();
                    break;
                }
            }
        }
        _ui.uiNum = UIMgr.uiNum;
        _ui.layerName = layerName;
        _ui.layerParent = layer;//存父亲
        layer['maxcount'] = layer.numChildren;
        if (!isOpen && !UIMgr.isOpening(uiname, layerName)) {
            UIMgr.openingUIArray[layerName].push(_ui);
        }
        if (!hasResToLoad) {
            _ui.isOpen = isOpen;
            _ui.onOpen();
        }
    }

    /**返回一个类对象，但还没有被实例（creatView 函数尚未被调用）*/
    private static __cr(uiname:UIBase0):UIBase0{
        let _ui:UIBase0;
        _ui = UIMgr.uiMap[uiname.name] = uiname;
        _ui.name = uiname.name;
        return _ui ;
    }

    public static getLayer(layerName:string):Box
    {
        return UIMgr._layers[layerName];
    }

    /**
     * 判断界面是否已经在指定的层处于打开状态
     * @param	uiname
     * @param	layerName
     */
    private static isOpening(uiname:UIBase0, layerName:string):Boolean
    {
        let arr:Array<any> = UIMgr.openingUIArray[layerName];
        if ( arr == null )
            return false;
        for (let i:number = 0; i < arr.length;i++) {
            if (arr[i].name==uiname.name)
                return true;
        }
        return false;
    }

    /**
     * 关闭指定的界面实例
     * @param	uiname
     */
    public static closeUI(uiname:any):UIBase0
    {
        if(!uiname) return;
        let _ui:UIBase0 = UIMgr.uiMap[uiname.name];
        if (_ui == null) return null;
        if (_ui.parent == null) return _ui;
        let layer:Box = _ui.parent as Box;
        let layerName:string = layer.name;
        let array:Array<any> = UIMgr.openingUIArray[layerName];
        let uiIndex:number = array.indexOf(_ui);
        if (uiIndex > -1) {
            array.splice(uiIndex, 1);
            _ui.onClose();
        }
        layer["maxcount"] = layer.numChildren ;
        return _ui;
    }
}