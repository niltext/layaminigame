/**
 * @author hjw
 * 配置文件
 */
var ConfigData = /** @class */ (function () {
    function ConfigData() {
    }
    ConfigData.init = function () {
    };
    /**声音表*/
    ConfigData.soundCfg = {};
    /**分享表*/
    ConfigData.shareData = [];
    return ConfigData;
}());
//# sourceMappingURL=ConfigData.js.map