/**
 * @author hjw
 * 平台配置管理
 */
var MouseManager = laya.events.MouseManager;
var PlatformMgr = /** @class */ (function () {
    function PlatformMgr() {
    }
    PlatformMgr.GetInstance = function () {
        if (this.instance == undefined) {
            this.instance = new PlatformMgr();
        }
        return this.instance;
    };
    PlatformMgr.prototype.init = function () {
        MouseManager.multiTouchEnabled = false;
        Laya.URL.basePath = "";
        if (Laya.Browser.onMiniGame) {
            wx.Bmob.initialize("5ea52f9df359ed6960d2fe7a92be1aac", "7b5267e42ea8d232fab27579bb22c794");
            wx.Bmob.User.auth().then(function (res) {
                console.log('微信登陆成功:', res);
            }).catch(function (err) {
                console.log(err);
            });
            wx.showShareMenu({
                withShareTicket: false
            });
            wx.onShareAppMessage({
                title: "",
                imageUrl: "",
                query: ""
            });
            wx.setKeepScreenOn({ keepScreenOn: true });
        }
    };
    return PlatformMgr;
}());
//# sourceMappingURL=PlatformMgr.js.map