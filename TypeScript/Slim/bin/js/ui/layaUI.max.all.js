var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var UIBase0 = laya.ui.UIBase0;
var ui;
(function (ui) {
    var hall;
    (function (hall) {
        var HallUI = /** @class */ (function (_super) {
            __extends(HallUI, _super);
            function HallUI() {
                return _super.call(this) || this;
            }
            HallUI.prototype.createChildren = function () {
                View.regComponent("custom.ButtonScaleI", custom.ButtonScaleI);
                _super.prototype.createChildren.call(this);
                this.createView(ui.hall.HallUI.uiView);
            };
            HallUI.uiView = { "type": "UIBase0", "props": { "width": 720, "renderType": "render", "height": 1560 }, "child": [{ "type": "Image", "props": { "y": 0, "x": 0, "top": 0, "skin": "common/white.png", "sizeGrid": "2,2,2,2", "right": 0, "name": "bg", "left": 0, "bottom": 0 } }, { "type": "Box", "props": { "y": 0, "x": 0, "name": "playerInfo" }, "child": [{ "type": "Image", "props": { "y": 0, "x": 0, "var": "head", "skin": "main/avtar.png" } }, { "type": "Image", "props": { "y": 0, "x": 92, "width": 317, "skin": "main/bg6.png", "height": 59 }, "child": [{ "type": "Label", "props": { "y": 6, "x": 60, "width": 117, "var": "money", "valign": "middle", "text": "58666", "height": 48, "fontSize": 30, "font": "SimHei", "color": "#000000", "bold": true, "align": "left" } }, { "type": "Label", "props": { "y": 7, "x": 183, "width": 131, "var": "charm", "valign": "middle", "text": "+666/秒", "height": 48, "fontSize": 30, "font": "SimHei", "color": "#000000", "bold": true, "align": "left" } }] }, { "type": "Image", "props": { "y": 59, "x": 93, "skin": "main/bg5.png" }, "child": [{ "type": "Label", "props": { "y": -1, "x": 9, "width": 82, "var": "CardLevel", "valign": "middle", "text": "Lv.30", "height": 39, "fontSize": 30, "font": "SimHei", "color": "#000000", "bold": true, "align": "left" } }] }] }, { "type": "Box", "props": { "y": 112, "x": 2, "name": "top" }, "child": [{ "type": "Image", "props": { "y": 70, "x": 199, "skin": "main/bg1.png" }, "child": [{ "type": "Label", "props": { "y": 4, "x": 9, "width": 298, "var": "time", "valign": "middle", "text": "坚持时间1小时10分10秒", "height": 31, "fontSize": 25, "font": "SimHei", "color": "#000000", "bold": true, "align": "center" } }] }, { "type": "Image", "props": { "y": 10, "x": 79, "skin": "main/bg4.png" }, "child": [{ "type": "Image", "props": { "y": -10, "x": 222, "var": "pointer", "skin": "main/pointer.png" } }] }, { "type": "Label", "props": { "y": 114, "width": 172, "var": "calorie", "text": "20千卡/秒", "strokeColor": "#ffffff", "stroke": 3, "height": 35, "fontSize": 30, "font": "SimHei", "color": "#000000", "align": "center" } }] }, { "type": "Box", "props": { "y": 308, "x": 0, "width": 720, "var": "playerRoot", "name": "playerRoot", "height": 881 } }, { "type": "Box", "props": { "y": 330, "x": 0, "name": "mid" }, "child": [{ "type": "Button", "props": { "y": 52, "var": "btn_accelerate", "stateNum": 1, "skin": "main/btn_jiasu.png", "left": 0, "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }, { "type": "Button", "props": { "y": 172, "var": "btn_getMoney", "stateNum": 1, "skin": "main/btn_free.png", "left": 0, "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }] }, { "type": "Box", "props": { "y": 1400, "x": 0, "var": "btmBox", "height": 160 }, "child": [{ "type": "Image", "props": { "y": 5, "skin": "main/bg3.png" } }, { "type": "Button", "props": { "y": 84, "x": 117, "var": "btn_levelSys", "stateNum": 1, "skin": "main/btn_lvup.png", "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }, { "type": "Button", "props": { "y": 84, "x": 605, "var": "btn_friend", "stateNum": 1, "skin": "main/btn_friend.png", "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }, { "type": "Image", "props": { "x": 227, "stroke": -239, "skin": "main/bg2.png" }, "child": [{ "type": "Label", "props": { "y": 14, "x": 16, "width": 234, "var": "weight", "valign": "middle", "text": "499斤", "strokeColor": "#000000", "stroke": 6, "height": 77, "fontSize": 50, "font": "SimHei", "color": "#ff0400", "bold": true, "align": "center" } }, { "type": "Label", "props": { "y": 95, "x": 12, "width": 242, "var": "leftCalorie", "valign": "middle", "text": "剩余200卡路里", "height": 59, "fontSize": 37, "font": "SimHei", "color": "#ffffff", "bold": true, "align": "center" } }] }] }] };
            return HallUI;
        }(UIBase0));
        hall.HallUI = HallUI;
    })(hall = ui.hall || (ui.hall = {}));
})(ui || (ui = {}));
(function (ui) {
    var hall;
    (function (hall) {
        var LevelUpUI = /** @class */ (function (_super) {
            __extends(LevelUpUI, _super);
            function LevelUpUI() {
                return _super.call(this) || this;
            }
            LevelUpUI.prototype.createChildren = function () {
                View.regComponent("custom.ButtonScaleI", custom.ButtonScaleI);
                _super.prototype.createChildren.call(this);
                this.createView(ui.hall.LevelUpUI.uiView);
            };
            LevelUpUI.uiView = { "type": "UIBase0", "props": { "width": 720, "height": 1560 }, "child": [{ "type": "Image", "props": { "var": "bg", "top": 0, "skin": "common/blank.png", "sizeGrid": "2,2,2,2", "right": 0, "left": 0, "bottom": 0 } }, { "type": "Box", "props": { "y": 377, "x": 0, "width": 720, "var": "myBox", "height": 806 }, "child": [{ "type": "Image", "props": { "y": 0, "x": 0, "skin": "levelUp/bg.png" } }, { "type": "Button", "props": { "y": 40, "x": 665, "var": "btnClose", "stateNum": 1, "skin": "levelUp/btn_close.png", "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }, { "type": "List", "props": { "y": 108, "x": 45, "var": "skillList", "spaceY": 15, "repeatY": 4, "renderType": "render" }, "child": [{ "type": "Box", "props": { "renderType": "render" }, "child": [{ "type": "Image", "props": { "skin": "levelUp/bar.png" } }, { "type": "Image", "props": { "y": 67, "x": 529, "skin": "levelUp/btn_lvup.png", "name": "btn_levelUp", "anchorY": 0.5, "anchorX": 0.5, "runtime": "custom.ButtonScaleI" } }, { "type": "Label", "props": { "y": 59, "x": 460, "width": 135, "valign": "middle", "text": "100", "name": "needMoney", "height": 60, "fontSize": 30, "color": "#000000", "bold": true, "align": "center" } }, { "type": "Label", "props": { "y": 45, "x": 125, "width": 63, "valign": "middle", "text": "说明", "name": "explain1", "height": 33, "fontSize": 20, "color": "#000000", "bold": true, "align": "center" } }, { "type": "Label", "props": { "y": 5, "x": 121, "width": 135, "valign": "middle", "text": "运动器材", "name": "equipment", "height": 39, "fontSize": 30, "color": "#000000", "bold": true, "align": "center" } }, { "type": "Label", "props": { "y": 80, "x": 125, "width": 63, "valign": "middle", "text": "说明", "name": "explain2", "height": 33, "fontSize": 20, "color": "#000000", "bold": true, "align": "center" } }] }] }] }] };
            return LevelUpUI;
        }(UIBase0));
        hall.LevelUpUI = LevelUpUI;
    })(hall = ui.hall || (ui.hall = {}));
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map