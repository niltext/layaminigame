/**
 * @author hjw
 * Tween、时间线工具集
 */
var Tween = laya.utils.Tween;
var Handler = Laya.Handler;
var Component = Laya.Component;
var Ease = Laya.Ease;
var TimeLine = Laya.TimeLine;
var EffectUtils = /** @class */ (function () {
    function EffectUtils() {
    }
    /**
     * 缓动打开弹窗
     * @param target
    */
    EffectUtils.openDialog = function (target, compH) {
        if (compH === void 0) { compH = null; }
        target.cacheAs = "none"; //"normal";
        target && (target.mouseEnabled = false);
        target.pivot(target.width / 2, target.height / 2);
        target.pos(EffectUtils.CURRENTWIDTH / UIMgr.scale / 2, EffectUtils.CURRENTHEIGHT / UIMgr.scale / 2);
        target.scale(0, 0);
        Tween.to(target, { scaleX: 1, scaleY: 1 }, 250, Ease.backOut, Handler.create(EffectUtils, function () {
            if (target) {
                target.mouseEnabled = true;
                target.cacheAs = "none";
                compH && compH.run();
            }
        }));
    };
    /**
     * 缓动关闭弹窗
     * @param target
    */
    EffectUtils.closeDialog = function (target, callBack) {
        if (callBack === void 0) { callBack = null; }
        target && (target.mouseEnabled = false);
        Tween.to(target, { scaleX: 0, scaleY: 0 }, 250, Ease.backIn, Handler.create(EffectUtils, function () {
            target && (target.mouseEnabled = true);
            callBack && callBack.run();
        }));
    };
    /**
     * 渐显渐隐
     * @param	target
     * @return
     */
    EffectUtils.emer = function (target, stayTime, delay) {
        if (delay === void 0) { delay = 0; }
        target.alpha = 0;
        var tl = new TimeLine();
        tl.reset();
        tl.to(target, { alpha: 0 }, delay)
            .to(target, { alpha: 1 }, 300)
            .to(target, { alpha: 1 }, stayTime)
            .to(target, { alpha: 0 }, 200);
        tl.play();
        //return tl;
    };
    EffectUtils.CURRENTWIDTH = 0;
    EffectUtils.CURRENTHEIGHT = 0;
    return EffectUtils;
}());
//# sourceMappingURL=EffectUtils.js.map