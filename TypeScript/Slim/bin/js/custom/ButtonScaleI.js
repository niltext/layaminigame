var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @author hjw
 * 按钮扩展组件
 */
var custom;
(function (custom) {
    var Button = laya.ui.Button;
    var Event = laya.events.Event;
    var ButtonScaleI = /** @class */ (function (_super) {
        __extends(ButtonScaleI, _super);
        function ButtonScaleI(skin, label) {
            if (skin === void 0) { skin = ""; }
            if (label === void 0) { label = ""; }
            var _this = _super.call(this, skin, label) || this;
            /**按钮点击播放声音类型（类型按表数据索引来定）**/
            _this.soundType = 0;
            /**鼠标或手指按下的持续时间**/
            _this.scaleDownTime = 100;
            /**鼠标或手指抬起的持续时间**/
            _this.scaleUpTime = 200;
            _this.anchorX = _this.anchorY = 0.5;
            _this.stateNum = 1;
            _this.initEvent();
            return _this;
        }
        /**初始化事件**/
        ButtonScaleI.prototype.initEvent = function () {
            this.on(Event.MOUSE_DOWN, this, this.scaleSmall);
            this.on(Event.MOUSE_UP, this, this.scaleNormal);
            this.on(Event.MOUSE_OUT, this, this.scaleNormal);
        };
        /**鼠标按下缩放效果**/
        ButtonScaleI.prototype.scaleSmall = function () {
            if (this.bIsMove)
                return;
            this.bIsMove = true;
            // let str:string;
            // if (this.soundType != 0 && ConfigData.soundCfg[this.soundType]){
            //     (str = ConfigData.soundCfg[this.soundType].src) && SoundManager.playSound(str);
            // }
            Tween.to(this, { scaleX: 1.08, scaleY: 1.08 }, this.scaleDownTime);
        };
        /**鼠标抬起放大恢复效果**/
        ButtonScaleI.prototype.scaleNormal = function () {
            var _this = this;
            Tween.to(this, { scaleX: 1, scaleY: 1 }, this.scaleUpTime, ButtonScaleI.backOut, Handler.create(this, function () {
                _this.bIsMove = false;
            }));
        };
        ButtonScaleI.backOut = function (t, b, c, d, s) {
            if (s === void 0) { s = 10; }
            return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
        };
        return ButtonScaleI;
    }(Button));
    custom.ButtonScaleI = ButtonScaleI;
})(custom || (custom = {}));
//# sourceMappingURL=ButtonScaleI.js.map