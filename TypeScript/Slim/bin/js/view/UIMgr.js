/**
 * @author hjw
 * 层级窗口管理
 */
var Box = laya.ui.Box;
var UIMgr = /** @class */ (function () {
    function UIMgr() {
    }
    UIMgr.GetInstance = function () {
        if (UIMgr.instance == undefined) {
            UIMgr.instance = new UIMgr();
        }
        return UIMgr.instance;
    };
    UIMgr.prototype.init = function () {
        UIMgr.scaleRatio = Laya.Browser.clientWidth / 720;
        UIMgr.REH = 720 * (Laya.Browser.clientHeight / Laya.Browser.clientWidth);
        UIMgr.AllLayer = new Box();
        UIMgr.AllLayer.mouseEnabled = true;
        Laya.stage.addChild(UIMgr.AllLayer);
        UIMgr.AllLayer.top = UIMgr.AllLayer.bottom = UIMgr.AllLayer.left = UIMgr.AllLayer.right = 0;
        UIMgr._layerNames = [UIMgr.LAYER_BG, UIMgr.LAYER_POP1, UIMgr.LAYER_POP2, UIMgr.LAYER_GUIDE, UIMgr.LAYER_TIPS, UIMgr.LAYER_LOADING, UIMgr.LAYER_SHARE];
        UIMgr._layers = {};
        UIMgr.uiMap = {};
        UIMgr.openingUIArray = {};
        var __layer = new Box();
        __layer.name = UIMgr.LAYER_BG;
        __layer.mouseThrough = true;
        __layer.mouseEnabled = true;
        __layer.left = __layer.top = __layer.right = __layer.bottom = 0;
        UIMgr.AllLayer.addChild(__layer);
        UIMgr._layers[UIMgr._layerNames[0]] = __layer;
        UIMgr.openingUIArray[UIMgr._layerNames[0]] = [__layer];
        for (var i = 1; i < UIMgr._layerNames.length; i++) {
            __layer = new Box();
            __layer.name = UIMgr._layerNames[i];
            __layer.mouseThrough = true;
            __layer.mouseEnabled = true;
            __layer.left = __layer.top = __layer.right = __layer.bottom = 0;
            UIMgr.AllLayer.addChild(__layer);
            UIMgr._layers[UIMgr._layerNames[i]] = __layer;
            UIMgr.openingUIArray[UIMgr._layerNames[i]] = [];
        }
        this.onStageResize();
    };
    UIMgr.prototype.onStageResize = function () {
        UIMgr.scale = Math.min(Laya.stage.width / Laya.stage.designWidth, Laya.stage.height / Laya.stage.designHeight);
        for (var key in UIMgr._layers) {
            // @ts-ignore
            var _layer = UIMgr._layers[key];
            _layer.scale(UIMgr.scale, UIMgr.scale);
        }
        //bgBox.scale(scale, scale);
        //console.log('缩放比例:' + scale);
        if (Laya.stage.width < Laya.stage.height) {
            EffectUtils.CURRENTWIDTH = Laya.stage.width;
            EffectUtils.CURRENTHEIGHT = Laya.stage.height;
        }
        else {
            EffectUtils.CURRENTWIDTH = Laya.stage.height;
            EffectUtils.CURRENTHEIGHT = Laya.stage.width;
        }
    };
    /**
     * 添加页面
     * @param uiname
     * @param layerName
     * @param isShowLoad 打开页面时是否显示loading动画
     * @return
     */
    UIMgr.openUI = function (uiname, layerName, isShowLoad) {
        if (layerName === void 0) { layerName = UIMgr.LAYER_POP2; }
        if (isShowLoad === void 0) { isShowLoad = true; }
        UIMgr.uiNum++;
        var _ui;
        var isOpen = !isShowLoad;
        if (UIMgr.isOpening(uiname, layerName)) {
            _ui = UIMgr.uiMap[uiname.name];
            _ui.isOpen = isOpen = false;
        }
        var closeUIName = UIMgr.autoCloseUI(layerName, false);
        UIMgr._createUI(uiname, layerName, isOpen);
        return closeUIName;
    };
    /**
     * 自动关闭层级中最后打开的界面
     * @param	layerName  层级名字
     * @param	isRealClose  是否真的关闭页面
     * @return
     */
    UIMgr.autoCloseUI = function (layerName, isRealClose) {
        var layer = UIMgr.getLayer(layerName);
        var maxCount = parseInt(layer['maxcount']);
        if (isNaN(maxCount) || maxCount < 1)
            maxCount = 1;
        if (UIMgr.openingUIArray[layerName].length >= maxCount) {
            var _ui = UIMgr.openingUIArray[layerName][maxCount - 1];
            var uiName = _ui.name;
            isRealClose && UIMgr.closeUI(_ui);
            return uiName;
        }
        return '';
    };
    UIMgr._createUI = function (uiname, layerName, isOpen) {
        if (isOpen === void 0) { isOpen = false; }
        var layer = UIMgr.getLayer(layerName);
        var _ui = UIMgr.uiMap[uiname.name];
        var hasResToLoad = false;
        if (_ui == null || !_ui.isloaded) {
            _ui = UIMgr.__cr(uiname);
            _ui.uiNum = UIMgr.uiNum;
            _ui.isCrOpen = true;
            _ui.layerName = layerName;
            _ui.layerParent = layer; //存父亲
            hasResToLoad = _ui.loadRes();
            _ui.isloaded = true;
        }
        else {
            for (var i = 0, sz = _ui.resList.length; i < sz; i++) {
                if (!Laya.loader.getRes(_ui.resList[i].url)) {
                    hasResToLoad = _ui.loadRes();
                    break;
                }
            }
        }
        _ui.uiNum = UIMgr.uiNum;
        _ui.layerName = layerName;
        _ui.layerParent = layer; //存父亲
        layer['maxcount'] = layer.numChildren;
        if (!isOpen && !UIMgr.isOpening(uiname, layerName)) {
            UIMgr.openingUIArray[layerName].push(_ui);
        }
        if (!hasResToLoad) {
            _ui.isOpen = isOpen;
            _ui.onOpen();
        }
    };
    /**返回一个类对象，但还没有被实例（creatView 函数尚未被调用）*/
    UIMgr.__cr = function (uiname) {
        var _ui;
        _ui = UIMgr.uiMap[uiname.name] = uiname;
        _ui.name = uiname.name;
        return _ui;
    };
    UIMgr.getLayer = function (layerName) {
        return UIMgr._layers[layerName];
    };
    /**
     * 判断界面是否已经在指定的层处于打开状态
     * @param	uiname
     * @param	layerName
     */
    UIMgr.isOpening = function (uiname, layerName) {
        var arr = UIMgr.openingUIArray[layerName];
        if (arr == null)
            return false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].name == uiname.name)
                return true;
        }
        return false;
    };
    /**
     * 关闭指定的界面实例
     * @param	uiname
     */
    UIMgr.closeUI = function (uiName) {
        if (!uiName)
            return;
        var _ui = UIMgr.uiMap[uiName.name];
        if (_ui == null)
            return null;
        if (_ui.parent == null)
            return _ui;
        var layer = _ui.parent;
        var layerName = layer.name;
        var array = UIMgr.openingUIArray[layerName];
        var uiIndex = array.indexOf(_ui);
        if (uiIndex > -1) {
            array.splice(uiIndex, 1);
            _ui.onClose();
        }
        layer["maxcount"] = layer.numChildren;
        return _ui;
    };
    /**背景层**/
    //public static let bgBox:Box;
    UIMgr.LAYER_BG = "layer_bg"; //UI背景
    UIMgr.LAYER_POP1 = "layer_pop1";
    UIMgr.LAYER_POP2 = "layer_pop2";
    UIMgr.LAYER_GUIDE = "layer_guide"; //引导层
    UIMgr.LAYER_TIPS = "layer_tips"; //弹窗层
    UIMgr.LAYER_LOADING = "layer_loading";
    UIMgr.LAYER_SHARE = "layer_share";
    UIMgr.uiNum = 0;
    return UIMgr;
}());
//# sourceMappingURL=UIMgr.js.map