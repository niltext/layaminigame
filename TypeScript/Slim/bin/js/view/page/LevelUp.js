var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var view;
(function (view) {
    var page;
    (function (page) {
        var Event = laya.events.Event;
        var LevelUp = /** @class */ (function (_super) {
            __extends(LevelUp, _super);
            function LevelUp() {
                var _this = _super.call(this) || this;
                _this.name = "LevelUp";
                _this.resList = [
                    { url: "res/atlas/levelUp.atlas", type: Laya.Loader.ATLAS }
                ];
                return _this;
            }
            LevelUp.prototype.onCreated = function () {
                _super.prototype.onCreated.call(this);
            };
            LevelUp.prototype.onOpen = function () {
                _super.prototype.onOpen.call(this);
                //添加到父容器
                _super.prototype.addToParent.call(this, this);
                EffectUtils.openDialog(this.myBox);
                this.btnClose.on(Event.CLICK, this, this.OnBtnClose);
                this.bg.on(Event.CLICK, this, this.OnBtnClose);
                this.Init();
            };
            LevelUp.onShowMe = function () {
                if (LevelUp.isOpening)
                    return;
                LevelUp.isOpening = true;
                if (LevelUp.instance == undefined) {
                    LevelUp.instance = new LevelUp();
                }
                UIMgr.openUI(LevelUp.instance, UIMgr.LAYER_POP2);
            };
            LevelUp.prototype.OnBtnClose = function () {
                this.bg.off(Event.CLICK, this, this.OnBtnClose);
                this.btnClose.off(Event.CLICK, this, this.OnBtnClose);
                EffectUtils.closeDialog(this.myBox, Handler.create(this, this.onTweenBtnClose));
            };
            LevelUp.prototype.onTweenBtnClose = function () {
                UIMgr.closeUI(this);
            };
            LevelUp.prototype.onClose = function () {
                LevelUp.isOpening = false;
                _super.prototype.onClose.call(this);
            };
            LevelUp.prototype.Init = function () {
                this.skillList.renderHandler = new Laya.Handler(this, this.ItemRender);
                this.RefreshList();
            };
            LevelUp.prototype.RefreshList = function () {
                var length = UpgradeSys.GetInstance().skillDataList.length;
                var list = [];
                list.length = length;
                this.skillList.dataSource = list;
            };
            LevelUp.prototype.ItemRender = function (cell, index) {
                var upgradeBtn = cell.getChildByName('btn_levelUp');
                var needMoney = cell.getChildByName('needMoney');
                var explain1 = cell.getChildByName('explain1');
                var equipment = cell.getChildByName('equipment');
                var explain2 = cell.getChildByName('explain2');
                var skillData = UpgradeSys.GetInstance().skillDataList[index];
                needMoney.text = skillData.needMoney + '';
                explain1.text = skillData.data.des1;
                explain2.text = skillData.data.des2;
                equipment.text = skillData.data.name;
                upgradeBtn.on(Laya.Event.CLICK, this, this.OnUpgradeClick, [index]);
            };
            LevelUp.prototype.OnUpgradeClick = function (index) {
                UpgradeSys.GetInstance().AddLv(index);
                this.RefreshList();
            };
            return LevelUp;
        }(ui.hall.LevelUpUI));
        page.LevelUp = LevelUp;
    })(page = view.page || (view.page = {}));
})(view || (view = {}));
//# sourceMappingURL=LevelUp.js.map