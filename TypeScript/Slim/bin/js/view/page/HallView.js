var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @author hjw
 * 主界面
 */
var view;
(function (view) {
    var page;
    (function (page) {
        var Event = laya.events.Event;
        var HallView = /** @class */ (function (_super) {
            __extends(HallView, _super);
            function HallView() {
                var _this = _super.call(this) || this;
                _this.name = "HallView";
                _this.resList = [
                    { url: "res/atlas/main.atlas", type: Laya.Loader.ATLAS }
                ];
                return _this;
            }
            HallView.prototype.onCreated = function () {
                _super.prototype.onCreated.call(this);
                this.Init();
            };
            HallView.prototype.onOpen = function () {
                _super.prototype.onOpen.call(this);
                //添加到父容器
                _super.prototype.addToParent.call(this, this);
                this.btmBox.y = UIMgr.REH - 160;
                this.btn_levelSys.on(Event.CLICK, this, page.LevelUp.onShowMe);
            };
            HallView.onShowMe = function () {
                if (HallView.isOpening)
                    return;
                HallView.isOpening = true;
                if (HallView.instance == undefined) {
                    HallView.instance = new HallView();
                }
                UIMgr.openUI(HallView.instance, UIMgr.LAYER_POP2);
            };
            HallView.prototype.onClose = function () {
                this.btn_levelSys.off(Event.CLICK, this, page.LevelUp.onShowMe);
                HallView.isOpening = false;
                _super.prototype.onClose.call(this);
            };
            /**
             * 创建界面时初始化
             */
            HallView.prototype.Init = function () {
                this.SetEventHandler();
                this.SetBtnHandler();
                this.RefreshView();
            };
            /**
             * 添加事件监听函数
             */
            HallView.prototype.SetEventHandler = function () {
                var _this = this;
                EventSys.GetInstance().AddHandler(Events.GoldNumChange, new Laya.Handler(this, this.RefreshMoney));
                EventSys.GetInstance().AddHandler(Events.HeartChange, new Laya.Handler(this, function () {
                    _this.RefreshExercise();
                    _this.RefreshHeart();
                }));
                EventSys.GetInstance().AddHandler(Events.ExerciseSpeedChange, new Laya.Handler(this, this.RefreshExercise));
                EventSys.GetInstance().AddHandler(Events.HeartTimeChange, new Laya.Handler(this, this.RefreshDuringTime));
                EventSys.GetInstance().AddHandler(Events.CalorieChange, new Laya.Handler(this, this.RefreshWeight));
                EventSys.GetInstance().AddHandler(Events.CharmChange, new Laya.Handler(this, this.RefreshCharm));
            };
            /**
             * 添加点击事件
             */
            HallView.prototype.SetBtnHandler = function () {
                this.playerRoot.on(Laya.Event.CLICK, this, function () {
                    PlayerInfo.GetInstance().ClickPerson();
                });
            };
            /**
             * 刷新界面显示
             */
            HallView.prototype.RefreshView = function () {
                this.RefreshCharm();
                this.RefreshDuringTime();
                this.RefreshExercise();
                this.RefreshMoney();
                this.RefreshWeight();
                this.RefreshHeart();
            };
            /**
             * 刷新金钱数量显示
             */
            HallView.prototype.RefreshMoney = function () {
                this.money.text = PlayerInfo.GetInstance().curGoldNum + '';
            };
            /**
             * 刷新魅力显示
             */
            HallView.prototype.RefreshCharm = function () {
                this.charm.text = PlayerInfo.GetInstance().curCharm + '/S';
            };
            /**
             * 刷新决心进度条
             */
            HallView.prototype.RefreshHeart = function () {
                this.pointer.x = 558 * PlayerInfo.GetInstance().curHeart / PlayerInfo.GetInstance().MaxHeart;
            };
            /**
             * 刷新剩余时间
             */
            HallView.prototype.RefreshDuringTime = function () {
                var time = Math.floor(PlayerInfo.GetInstance().curHeartTime / 1000);
                var hour = Math.floor(time / 3600);
                var min = Math.floor((time % 3600) / 60);
                var sec = time % 60;
                this.time.text = '坚持时间' + hour + '小时' + min + '分' + sec + '秒';
            };
            /**
             * 刷新剩余体重
             */
            HallView.prototype.RefreshWeight = function () {
                this.weight.text = PlayerInfo.GetInstance().GetWeightStr() + '斤';
                this.leftCalorie.text = '剩余' + Math.ceil(PlayerInfo.GetInstance().curCalorie) + '卡路里';
            };
            /**
             * 刷新锻炼速度显示
             */
            HallView.prototype.RefreshExercise = function () {
                this.calorie.text = Math.ceil(PlayerInfo.GetInstance().curExercise) + '/S';
            };
            return HallView;
        }(ui.hall.HallUI));
        page.HallView = HallView;
    })(page = view.page || (view.page = {}));
})(view || (view = {}));
//# sourceMappingURL=HallView.js.map