// 程序入口
var Main = /** @class */ (function () {
    function Main() {
        Laya.MiniAdpter.init();
        var realHeight = 720 * (Laya.Browser.clientHeight / Laya.Browser.clientWidth);
        Laya.init(720, realHeight);
        this.initStage();
        this.loadResource();
        PlatformMgr.GetInstance().init();
    }
    //初始化stage
    Main.prototype.initStage = function () {
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_WIDTH;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
    };
    //加载资源
    Main.prototype.loadResource = function () {
        var uiResArr = [
            { url: "res/atlas/common.atlas", type: Laya.Loader.ATLAS },
            { url: "res/atlas/main.atlas", type: Laya.Loader.ATLAS },
            { url: "res/atlas/levelUp.atlas", type: Laya.Loader.ATLAS }
        ];
        Laya.loader.load(uiResArr, Laya.Handler.create(this, function () {
            UIMgr.GetInstance().init();
            PlayerInfo.GetInstance().Init();
            view.page.HallView.onShowMe();
            // UIMgr.closeUI(view.page.HallView.instance);
        }));
    };
    return Main;
}());
new Main();
//# sourceMappingURL=Main.js.map