var NetSys = /** @class */ (function () {
    function NetSys() {
        this.Init();
    }
    NetSys.prototype.Init = function () {
    };
    /**
     * 建立链接，设置监听函数
     */
    NetSys.prototype.InitSocket = function () {
        this.socket = new Laya.Socket();
        this.socket.on(Laya.Event.OPEN, this, function () {
            console.log('connect suc');
        });
        this.socket.on(Laya.Event.CLOSE, this, function () {
            console.log('connect close');
        });
        this.socket.on(Laya.Event.ERROR, this, function () {
            console.error('connect fail');
        });
        this.socket.on(Laya.Event.MESSAGE, this, function () {
            console.log('receive message');
        });
        this.socket.connectByUrl(this.tcpUrl);
    };
    return NetSys;
}());
//# sourceMappingURL=NetSys.js.map