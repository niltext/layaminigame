/**
 * 消息队列
 */
var EventSys = /** @class */ (function () {
    function EventSys() {
        this.Init();
    }
    EventSys.GetInstance = function () {
        if (this.instance == undefined) {
            this.instance = new EventSys();
        }
        return this.instance;
    };
    EventSys.prototype.Init = function () {
        this.handleDic = {};
        this.eventList = [];
        Laya.timer.frameLoop(1, this, this.Update);
    };
    /**
     * 添加监听函数
     */
    EventSys.prototype.AddHandler = function (eventId, handle) {
        if (this.handleDic[eventId] == undefined) {
            this.handleDic[eventId] = [];
        }
        this.handleDic[eventId].push(handle);
    };
    /**
     * 发送消息
     */
    EventSys.prototype.AddEvent = function (eventId) {
        this.eventList.push(eventId);
    };
    /**
     * 发送消息，立刻执行
     */
    EventSys.prototype.AddEventNow = function (eventId) {
        var handleList = this.handleDic[eventId];
        if (handleList == undefined)
            return;
        for (var i = 0; i < handleList.length; i++) {
            try {
                handleList[i].run();
            }
            catch (error) {
                console.error('event error ', eventId, error);
            }
        }
    };
    /**
     * 移除一个监听事件
     */
    EventSys.prototype.RemoveHandler = function (eventId, handle) {
        if (this.handleDic[eventId] == undefined) {
            return;
        }
        var index = this.handleDic[eventId].indexOf(handle);
        if (index > -1) {
            this.handleDic[eventId].splice(index, 1);
        }
    };
    EventSys.prototype.Update = function () {
        if (this.eventList.length > 0)
            console.log('eventList ', this.eventList);
        while (this.eventList.length > 0) {
            var eventId = this.eventList.pop();
            var handleList = this.handleDic[eventId];
            if (handleList == undefined)
                return;
            for (var i = 0; i < handleList.length; i++) {
                try {
                    handleList[i].run();
                }
                catch (error) {
                    console.error('event error ', eventId, error);
                }
            }
        }
    };
    return EventSys;
}());
/**
 * 事件类型
 */
var Events;
(function (Events) {
    /** 锻炼速度改变*/
    Events[Events["ExerciseSpeedChange"] = 0] = "ExerciseSpeedChange";
    /** 卡路里改变*/
    Events[Events["CalorieChange"] = 1] = "CalorieChange";
    /** 金钱数量改变*/
    Events[Events["GoldNumChange"] = 2] = "GoldNumChange";
    /** 决心改变*/
    Events[Events["HeartChange"] = 3] = "HeartChange";
    /** 持续时间改变*/
    Events[Events["HeartTimeChange"] = 4] = "HeartTimeChange";
    /**
     * 魅力改变
     */
    Events[Events["CharmChange"] = 5] = "CharmChange";
    Events[Events["Count"] = 6] = "Count";
})(Events || (Events = {}));
//# sourceMappingURL=EventSys.js.map