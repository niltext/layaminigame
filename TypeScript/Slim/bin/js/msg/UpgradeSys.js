/**
 * 升级系统
 */
var UpgradeSys = /** @class */ (function () {
    function UpgradeSys() {
        //测试数据
        this.skillList = [
            {
                name: '器材',
                baseMoney: 100,
                addMoney: 20,
                des1: '器材1',
                des2: '器材2',
                addValue: 1,
            },
            {
                name: '沐浴房',
                baseMoney: 200,
                addMoney: 10,
                des1: '沐浴房1',
                des2: '沐浴房2',
                addValue: -0.01,
            },
            {
                name: '会员卡',
                baseMoney: 300,
                addMoney: 10,
                des1: '会员卡1',
                des2: '会员卡2',
                addValue: 10000,
            },
            {
                name: '助教',
                baseMoney: 400,
                addMoney: 5,
                des1: '助教1',
                des2: '助教2',
                addValue: 0.1,
            }
        ];
        this.baseEquipCoe = 5;
        this.baseHeartDec = 1;
        this.baseMaxHeartTime = 3600000;
        this.baseCharmCoe = 1;
        this.Init();
    }
    Object.defineProperty(UpgradeSys.prototype, "equipCoe", {
        /**
         * 器材增加的运动量
         */
        get: function () {
            return this.skillDataList[0].attr + this.baseEquipCoe;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpgradeSys.prototype, "heartDec", {
        /**
         * 沐浴房增加的决心衰减速度
         */
        get: function () {
            return this.skillDataList[1].attr + this.baseHeartDec;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpgradeSys.prototype, "maxHeartTime", {
        /**
         * 会员卡增加的持续时间
         */
        get: function () {
            return this.skillDataList[2].attr + this.baseMaxHeartTime;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UpgradeSys.prototype, "charmCoe", {
        /**
         * 助教增加的魅力
         */
        get: function () {
            return this.skillDataList[3].attr + this.baseCharmCoe;
        },
        enumerable: true,
        configurable: true
    });
    UpgradeSys.GetInstance = function () {
        if (this.instance == undefined) {
            this.instance = new UpgradeSys();
        }
        return this.instance;
    };
    UpgradeSys.prototype.Init = function () {
        this.skillDataList = [];
        for (var i = 0; i < this.skillList.length; i++) {
            this.skillDataList.push(new SkillData(this.skillList[i]));
        }
    };
    UpgradeSys.prototype.AddLv = function (index) {
        if (index > -1 && index < this.skillDataList.length) {
            var skillData = this.skillDataList[index];
            var costMoneyNum = skillData.needMoney;
            if (PlayerInfo.GetInstance().CostGold(costMoneyNum)) {
                this.skillDataList[index].lv++;
                switch (index) {
                    case 0:
                        EventSys.GetInstance().AddEvent(Events.ExerciseSpeedChange);
                        break;
                    case 3:
                        EventSys.GetInstance().AddEvent(Events.CharmChange);
                        break;
                    default:
                        break;
                }
            }
        }
    };
    return UpgradeSys;
}());
var SkillData = /** @class */ (function () {
    function SkillData(data) {
        this.data = data;
        this.lv = 0;
    }
    Object.defineProperty(SkillData.prototype, "lv", {
        get: function () {
            return this._lv;
        },
        set: function (value) {
            if (this._lv != value) {
                this._lv = value;
                this.attr = this.data.addValue * this.lv;
                this.needMoney = this.data.addMoney * this.lv + this.data.baseMoney;
            }
        },
        enumerable: true,
        configurable: true
    });
    return SkillData;
}());
//# sourceMappingURL=UpgradeSys.js.map