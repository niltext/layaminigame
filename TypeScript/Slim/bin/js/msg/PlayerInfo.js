/**
 * 存放玩家信息（等级、经验、金钱等等）
 */
var PlayerInfo = /** @class */ (function () {
    function PlayerInfo() {
        //一些临时常量
        /** 决心最大值*/
        this.MaxHeart = 100;
        /** 点击决心增量*/
        this.HeartAddNum = 20;
        /** 基础运动量*/
        this.BaseExercise = 10;
        /** 阻力*/
        this.Bound = 20;
        /** 时间点击增量*/
        this.TimeAddNum = 5000;
        this.updatePerSecTime = 0;
        // this.Init()
    }
    Object.defineProperty(PlayerInfo.prototype, "curExercise", {
        /** 当前运动速度*/
        get: function () {
            var coe = this.curHeart / this.Bound > 1 ? this.curHeart / this.Bound : 1;
            return this.BaseExercise * coe * UpgradeSys.GetInstance().equipCoe;
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(PlayerInfo.prototype, "curCharm", {
        /**
         * 当前魅力（决定每秒获取多少金钱）
         */
        get: function () {
            return Math.floor((500 - this.curWeight) * UpgradeSys.GetInstance().charmCoe);
        },
        enumerable: true,
        configurable: true
    });
    PlayerInfo.GetInstance = function () {
        if (this.instance == undefined) {
            this.instance = new PlayerInfo();
        }
        return this.instance;
    };
    PlayerInfo.prototype.Init = function () {
        this.curLv = 1;
        this.curWeight = 499;
        this.curHeart = 0;
        this.curCalorie = 500;
        this.curMaxCalorie = 500;
        this.curHeartTime = 0;
        this.curGoldNum = 0;
        Laya.timer.frameLoop(1, this, this.Update);
    };
    /**
     * 增加决心
     */
    PlayerInfo.prototype.AddHeart = function (addNum) {
        this.curHeart += addNum;
        if (this.curHeart > this.MaxHeart) {
            var outNum = this.curHeart - this.MaxHeart;
            var outTime = Math.floor(outNum / this.HeartAddNum * this.TimeAddNum);
            this.curHeartTime += outTime;
            this.curHeart = this.MaxHeart;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange);
        }
        EventSys.GetInstance().AddEvent(Events.HeartChange);
    };
    /**
     * 卡路里增加
     */
    PlayerInfo.prototype.AddCalorie = function (changeValue) {
        if (changeValue <= 0)
            return;
        this.curCalorie += changeValue;
        while (this.curCalorie > this.curMaxCalorie) {
            this.curWeight++;
            this.curCalorie -= this.curMaxCalorie;
            this.curMaxCalorie = 500;
            EventSys.GetInstance().AddEvent(Events.CharmChange);
        }
        EventSys.GetInstance().AddEvent(Events.CalorieChange);
    };
    /**
     * 卡路里减少
     * @param changeValue
     */
    PlayerInfo.prototype.DecCalorie = function (changeValue) {
        if (changeValue <= 0)
            return;
        this.curCalorie -= changeValue;
        while (this.curCalorie <= 0) {
            this.curWeight--;
            this.curMaxCalorie = 500;
            this.curCalorie += this.curMaxCalorie;
            EventSys.GetInstance().AddEvent(Events.CharmChange);
        }
        EventSys.GetInstance().AddEvent(Events.CalorieChange);
    };
    /**
     * 增加金钱
     */
    PlayerInfo.prototype.AddGold = function (addNum) {
        this.curGoldNum += addNum;
        EventSys.GetInstance().AddEvent(Events.GoldNumChange);
    };
    /**
     * 花费金钱，返回是否扣除成功
     */
    PlayerInfo.prototype.CostGold = function (costNum) {
        if (this.curGoldNum < costNum)
            return false;
        this.curGoldNum -= costNum;
        EventSys.GetInstance().AddEvent(Events.GoldNumChange);
        return true;
    };
    /**
     * 点击中间的胖子增加决心
     */
    PlayerInfo.prototype.ClickPerson = function () {
        if (this.curHeartTime > 0) {
            this.curHeartTime += this.TimeAddNum;
            this.curHeartTime = this.curHeartTime > UpgradeSys.GetInstance().maxHeartTime ?
                UpgradeSys.GetInstance().maxHeartTime : this.curHeartTime;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange);
        }
        else {
            this.AddHeart(this.HeartAddNum);
        }
    };
    /**
     * 获取当前体重（显示在主界面上）
     */
    PlayerInfo.prototype.GetWeightStr = function () {
        var weight = this.curWeight;
        if (this.curCalorie == this.curMaxCalorie)
            weight++;
        var weightStr = this.curWeight + '';
        var calorieStr = (this.curCalorie / this.curMaxCalorie) + '';
        var strList = calorieStr.split('.');
        if (strList.length > 1) {
            calorieStr = strList[1];
            calorieStr = calorieStr.substr(0, 2);
            weightStr = weightStr + '.' + calorieStr;
        }
        return weightStr;
    };
    /**
     * 每帧减少决心
     */
    PlayerInfo.prototype.UpdateDecHeart = function () {
        if (this.curHeartTime > 0) {
            this.curHeartTime -= Laya.timer.delta;
            this.curHeartTime = this.curHeartTime < 0 ? 0 : this.curHeartTime;
            EventSys.GetInstance().AddEvent(Events.HeartTimeChange);
        }
        else if (this.curHeart > 0) {
            var decNum = UpgradeSys.GetInstance().heartDec / 1000 * Laya.timer.delta;
            this.curHeart -= decNum;
            this.curHeart = this.curHeart < 0 ? 0 : this.curHeart;
            EventSys.GetInstance().AddEvent(Events.HeartChange);
        }
    };
    /**
     * 每秒减少卡路里
     */
    PlayerInfo.prototype.UpdateDecCalorie = function () {
        this.DecCalorie(this.curExercise);
    };
    /**
     * 每秒增加金钱
     */
    PlayerInfo.prototype.UpdateAddGold = function () {
        this.AddGold(this.curCharm);
    };
    /**
     * 每秒执行一次
     */
    PlayerInfo.prototype.UpdatePerSec = function () {
        this.UpdateDecCalorie();
        this.UpdateAddGold();
    };
    PlayerInfo.prototype.Update = function () {
        if (Laya.timer.currTimer - this.updatePerSecTime >= 1000) {
            this.UpdatePerSec();
            this.updatePerSecTime = Laya.timer.currTimer;
        }
        this.UpdateDecHeart();
    };
    return PlayerInfo;
}());
//# sourceMappingURL=PlayerInfo.js.map